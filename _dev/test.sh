#!/usr/bin/env bash

while getopts h:p: option
do
case "${option}"
in
h) CLIENT_HOST=${OPTARG};;
p) CLIENT_PORT=${OPTARG};;
esac
done


#CLIENT_IP=$1
#CLIENT_PORT=$2

echo "streaming to $CLIENT_HOST:$CLIENT_PORT"

#gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=$CLIENT_IP port=5000
