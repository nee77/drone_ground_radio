#!/usr/bin/env bash


gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5000
