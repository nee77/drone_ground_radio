


### nodejs request
https://github.com/request/request/tree/master/examples



## VLC

sud


    sudo apt-get install vlc libdvdcss2






Трансляция в UDP порт
    
    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1280,height=720,framereate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5000




cvlc -vvv udp://@:5000 --sout '#rtp{sdp=rtsp://0.0.0.0:8554/test}' 


cvlc -vvv udp://@:5000 --sout '#rtp{dst=192.168.0.79,port=1234,sdp=rtsp://192.168.0.79:8554/test.sdp}' 


cvlc -vvv udp://@:5000:chroma="H264":width=1280:height=720:fps=30 --sout '#rtp{sdp=rtsp://:8554/test}' :demux=h264

cvlc -vvv udp://@:5000 --sout '#rtp{sdp=rtsp://:8554/test}'

cvlc -vvv udp://@:5000:chroma="H264":width=1280:height=720:fps=30 --sout '#rtp{sdp=rtsp://:8554/t1}'

vlc -vvv udp://@:5000 --sout '#standard{access=http,mux=ogg,dst=192.168.0.79:8080}' 
       
       
vlc -vvv udp://@:5000 --sout '#rtp{dst=192.168.0.79,port=1234,sdp=rtsp://:8554/test1}' 

cvlc -vvv udp://@:5000 --sout '#rtp{sdp=rtsp://:8554}' :demux=h264

cvlc -vvv udp://@:5000 --sout '#standard{access=http,mux=ts,dst=:8090}' :demux=h264








gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1280,height=720,framereate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=127.0.0.1 port=5000


gst-launch-1.0 videotestsrc ! video/x-h264,width=1280,height=720,framereate=30/1,profile=high ! udpsink host=127.0.0.1 port=5000


gst-launch-1.0 -vv -e autovideosrc ! queue ! x264enc speed-preset=1 ! h264parse ! mpegtsmux ! rtpmp2tpay ! udpsink host=127.0.0.1 port=5000




cvlc -vvv --network-caching 0 --demux h264 udp://@:5000 --sout '#rtp{sdp=rtsp://:8554/t1}' :demux=h264


cvlc -vvv udp://@:5000 --sout '#transcode{vcodec=h264,vb=800,acodec=none,samplerate=44100}:rtp{sdp=rtsp://:8554/t1}' :demux=h264


cvlc -vvv udp://@:5000 --sout '#transcode{vcodec=h264,}:rtp{sdp=rtsp://:8554/t1}' 


raspivid -o - -n -fps 15 -w 1280 -h 720 -t 0 | cvlc -vvv --network-caching 0 stream:///dev/stdin --sout '#rtp{sdp=rtsp://:8554/t1}' :demux=h264 :h264-fps=15



Raspi-live
https://github.com/jaredpetersen/raspi-live

4 способа трансляции
http://c.wensheng.org/2017/05/18/stream-from-raspberrypi/

MJPEG streamer в браузер
https://docs.dataplicity.com/docs/stream-live-video-from-your-pi





## Видеотрансляция

локальная трансляция в UDP порт

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1280,height=720,framereate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5000

трансляция на RTSP сервер

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=640, height=360,framerate=30/1,profile=high ! h264parse ! flvmux ! rtmpsink location="rtmp://video.gcs.uno:1935/test11011/station_x live=1" 


Локальный RTSP с большой задержкой. Каждое новое устройство сильно тормозит общий поток
вещатель        

    /opt/vc/bin/raspivid -o - -t 0 -hf -w 640 -h 360 -fps 25 | cvlc -vvv stream:///dev/stdin --sout '#rtp{sdp=rtsp://:8554/t1}' :demux=h264
    
!!! проверить скорость

    raspivid -o - -n -fps 15 -w 1280 -h 720 -t 0 | cvlc -vvv --network-caching 0 stream:///dev/stdin --sout '#rtp{sdp=rtsp://:8554/t1}' :demux=h264 :h264-fps=15
    
    
приемник

    rtsp://192.168.0.71:8554/t1


//с usb веб-камеры:

    cvlc v4l2:///dev/video0 --sout '#transcode{vcodec=h264,acodec=none}:rtp{sdp=rtsp://:8554/}'





gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=640, height=360,framerate=30/1,profile=high ! h264parse ! flvmux ! udpsink host=192.168.0.28 port=5000



Killing process
http://krasimirtsonev.com/blog/article/Nodejs-managing-child-processes-starting-stopping-exec-spawn



gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5000




## Установка RethinkDB ##

Увеличиваем размер swap файла (Raspian Jessie)

    sudo nano /etc/dphys-swapfile

Сделаем 1Гб поменяв значение переменной

    CONF_SWAPSIZE=1024

Запускаем активацию swap (первая команда может занять продолжительное время)

    sudo dphys-swapfile setup
    sudo dphys-swapfile swapon

Перегружаемся

    sudo reboot

Обновляем дистрибутив

    sudo apt-get update && sudo apt-get upgrade -y

Установка зависимостей

    sudo apt-get install g++ protobuf-compiler libprotobuf-dev libboost-dev curl m4 wget libssl-dev

Скачаем и распакуем дистрибутив

    wget https://download.rethinkdb.com/dist/rethinkdb-2.3.5.tgz
    tar xf rethinkdb-2.3.5.tgz
    cd rethinkdb-2.3.5

Запустим установку

    ./configure --with-system-malloc --allow-fetch
    make ALLOW_WARNINGS=1

При возникновении ошибки:

    mk/support/build.mk:51: recipe for target 'external/zlib_1.2.8' failed

Открываем файл

    nano mk/support/pkg/zlib.sh

и меняем

    src_url=http://zlib.net/zlib-$version.tar.gz

на

    src_url=http://zlib.net/fossils/zlib-$version.tar.gz

собираем пакет

    sudo make install ALLOW_WARNINGS=1

Установка драйвера для NodeJS

    sudo npm install rethinkdb -g

Запускаем rethinkdb

    rethinkdb

Возвращаем на место настройки swap файла



