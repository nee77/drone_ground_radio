#!/usr/bin/env bash


gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1280,height=720,framereate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.1.34 port=5000

# udpsink host=$MULTICAST_IP_ADDR port=$VIDEO_UDP_PORT auto-multicast=true