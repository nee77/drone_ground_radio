webix.ready( () => {

    webix.ui({
        type:"line",
        rows: [
            {
                template: 'Video RX: #video_rx#'
                        + '<br/>Video stream: #video#'
                        + '<br/>MAVLink stream: #mavlink#'
                        + '<br/>Your IP: #client_ip#'
                ,data: {
                    video: '---'
                    ,mavlink: '---'
                    ,client_ip: '---'
                    ,video_rx: '---'
                }
                ,height: 100
                ,id: 'tpl:info'
            }
            ,{
                view: 'form'
                ,id: 'form:streams'
                ,elements: [
                    {
                        view: 'text', name: 'video_dst', placeholder: 'HOST:PORT for video streaming', labelWidth: 0, label: ''
                    }
                    ,{
                        view: 'text', name: 'mavlink_dst', placeholder: 'HOST:PORT for mavlink streaming', labelWidth: 0, label: ''
                    }
                ]
            }
            ,{
                view: 'button', id: 'btn:start', label: 'START streaming'
            }
            ,{
                view: 'button', id: 'btn:stop', label: 'STOP streaming', type: 'danger'
            }
            ,{}
        ]
    });


    const socket_io = io();

    const tpl_info = $$('tpl:info')
         ,form_streams = $$('form:streams')
         ,btn_start = $$('btn:start')
         ,btn_stop = $$('btn:stop');


    // События соединения socket.io
    socket_io.on('connect', function(){
        console.log('socket.io CONNECTED');
    });
    socket_io.on('disconnect', function(){
        console.log('socket.io DISCONNECTED');
    });

    socket_io.on('status', function(status){
        //console.log('Server status:');
        //console.log(status);
        tpl_info.setValues(status, true);
        console.log(status);
    });

    socket_io.on('streams_dst', function(streams){
        //console.log('Server status:');
        console.log(streams);

        form_streams.setValues(streams);
    });


    btn_start.attachEvent('onItemClick', function(){

        btn_start.disable();

        let streams_dst = form_streams.getValues();

        socket_io.emit('btn_start', streams_dst, (resp) => {
            //console.log('Server answer:');
            //console.log(data);

            btn_start.enable();

            if( 'OK' !== resp.video ){
                webix.message({type: 'error', text: 'Failed to start VIDEO streaming'});
            }

            if( 'OK' !== resp.mavlink ){
                webix.message({type: 'error', text: 'Failed to start MAVLink streaming'});
            }

        });

    });


    btn_stop.attachEvent('onItemClick', function() {

        btn_stop.disable();

        socket_io.emit('btn_stop', 'stop', (resp) => {

            btn_stop.enable();

            if( 'OK' !== resp.video ){
                webix.message({type: 'error', text: 'Failed to stop VIDEO streaming'});
            }

            if( 'OK' !== resp.mavlink ){
                webix.message({type: 'error', text: 'Failed to stop MAVLink streaming'});
            }

        });

    });



});