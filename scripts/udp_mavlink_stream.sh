#!/usr/bin/env bash

while getopts h:p: option
do
case "${option}"
in
h) CLIENT_HOST=${OPTARG};;
p) CLIENT_PORT=${OPTARG};;
esac
done


echo "streaming mavlink to $CLIENT_HOST:$CLIENT_PORT"


mavproxy.py --master=/dev/ttyS0  --out=udpout:$CLIENT_HOST:$CLIENT_PORT --daemon
