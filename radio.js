'use strict';

// Основной скрипт радиомодуля


const http = require('http');
const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const app = express();
const server = new http.Server(app);

// Загрузка модулей
const mcpadc = require('mcp-spi-adc')
     ,OnOff = require('onoff').Gpio
     ,redis = require('redis');
    //,Gpio = require('pigpio').Gpio

const { spawn } = require('child_process');
const psTree = require('ps-tree');


// Общие параметры конфигурации
const config = require('./config/config')
    // Распиновка
    ,wires = require('./config/pi_wiring.js');


// static files init
app.use(express.static(__dirname + '/web-ui'));


// JSON parse init
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// TODO запуск видео и телеметрии
//     локальный UDP
//     локальный RTSP/TCP
//     интернет RTSP/TCP
//


// TODO
// GPIO 6, 13 - OPTO switch погодной станции
// GPIO 26 - светодиод состояния
// GPIO 27 - реле питания (или переключатель видеосигнала)
// ADC 0 - напряжение указателя ветра



// Redis client init
const redisClient = redis.createClient({ host: config.REDIS_HOST, port: config.REDIS_PORT });
redisClient.on('ready',function() { console.log("Redis is ready"); });
redisClient.on('error',function() { console.log("Error in Redis"); });

// Socket.io server init
const io_server = require('socket.io')(server);


// Инициализация пинов управления
const relay_video_rx = new OnOff(wires.GPIO.RELAY_VIDEO_RX, 'high')
     ,status_led = new OnOff(wires.GPIO.STATUS_LED, 'high');




let video_process = null;
let mavlink_process = null;

let video_start_in_progress = false;
let mavlink_start_in_progress = false;

// Состояние процессов
let video_status = 'stopped';
let mavlink_status = 'stopped';

const VIDEO_RX_STATE_OFF = 1;
const VIDEO_RX_STATE_ON = 0;

let video_rx_state = VIDEO_RX_STATE_OFF;

// Сразу выключаем питание видеоприемника
relay_video_rx.write(video_rx_state, function(){});


//
// Уничтожитель процессов
const kill = function (pid, signal, callback) {

    signal   = signal || 'SIGKILL';
    callback = callback || function () {};

    psTree(pid, function (err, children) {
        [pid].concat(
            children.map(function (p) {
                return p.PID;
            })
        ).forEach(function (tpid) {
            try { process.kill(tpid, signal) }
            catch (ex) { }
        });
        callback();
    });

};


let emit_status = function(){
    // Отправляем текущее состояние
    io_server.sockets.emit('status', {
        video: video_status
        ,mavlink: mavlink_status
        ,video_rx: video_rx_state === VIDEO_RX_STATE_ON ? 'On' : 'Off'
    });
};


const start_video = function(destination, callback){ // { host: HOST, port: PORT }
    if( video_start_in_progress ) return;

    let process_confirmed = false;

    try {

        if( !callback ){
            callback = function(state){};
        }

        video_start_in_progress = true;
        video_status = 'start streaming to ' + destination.host + ':' + destination.port;
        emit_status();

        console.log('starting video to ' + destination.host + ':' + destination.port);


        const start_process = function(){
            console.log('starting video process');

            video_process = spawn("/home/pi/ground_radio/scripts/udp_video_stream.sh", ['-h', destination.host, '-p', destination.port], {killSignal: 'SIGINT'});

            video_process.stdout.on('data', (data) => {
                console.log(`stdout: ${data}`);

                if( !process_confirmed ){
                    video_start_in_progress = false;
                    video_status = 'ACTIVE streaming to ' + destination.host + ':' + destination.port;
                    process_confirmed = true;
                    emit_status();
                    callback('OK');
                }

            });

            video_process.stderr.on('data', (data) => {
                console.log(`stderr: ${data}`);

                if( !process_confirmed ){
                    video_start_in_progress = false;
                    video_status = 'error';
                    process_confirmed = true;
                    emit_status();
                    callback('error');
                }

            });

            video_process.on('close', (code) => {
                console.log(`child process exited with code ${code}`);

                // code = 255 failed to start

                if( !process_confirmed ){
                    video_start_in_progress = false;
                    process_confirmed = true;
                    callback('closed');
                }

                video_status = (code > 1 ? 'error ' + code : 'stopped');
                emit_status();
            });

            /*
            video_process.on('exit', (code, signal) => {
                console.log(`Exit code is: ${code}`);
                console.log(`Signal code is: ${signal}`);

                if( !process_confirmed ){
                    video_start_in_progress = false;
                    process_confirmed = true;
                    callback('exit');
                }

                video_status = 'stopped';
                emit_status();
            });
            */

        };

        const restart_process = function(){
            console.log('stopping video');

            video_process.kill('SIGSTOP');
            kill(video_process.pid, 'SIGKILL', function(){
                video_process = null;
                setTimeout(start_process, 200);
            });
        };


        // Если видеоприемник выключен
        if( video_rx_state === VIDEO_RX_STATE_OFF ){
            // Включаем его
            console.log('RX is off. Switching RX on...');
            video_rx_state = VIDEO_RX_STATE_ON;
            relay_video_rx.write(video_rx_state, function(){
                // И через 5 секунд стартуем процесс трансляции
                emit_status();

                console.log('waiting RX for some time...');
                setTimeout(start_process, 10000);
            });
        } else {
            if( video_process ){
                restart_process();
            }
            else {
                start_process();
            }
        }
    }
    catch (e){
        video_start_in_progress = false;
        video_status = 'error 0';
        emit_status();
        callback('error');
    }


};

const stop_video = function(callback){
    console.log('stopping video');

    if( video_process ){
        video_process.kill('SIGSTOP');
        kill(video_process.pid, 'SIGKILL', function(){
            video_process = null;
            video_status = 'stopped';

            video_rx_state = VIDEO_RX_STATE_OFF;
            relay_video_rx.write(video_rx_state, function(){
                // И через 5 секунд стартуем процесс трансляции
                emit_status();
                callback('OK');
            });
        });
    } else {
        video_process = null;
        video_status = 'stopped';

        video_rx_state = VIDEO_RX_STATE_OFF;
        relay_video_rx.write(video_rx_state, function(){
            // И через 5 секунд стартуем процесс трансляции
            emit_status();
            callback('OK');
        });
    }
};



const start_mavlink = function(destination, callback){ // { host: HOST, port: PORT }
    if( mavlink_start_in_progress ) return;

    let process_confirmed = false;

    try {

        if( !callback ){
            callback = function(state){};
        }

        mavlink_start_in_progress = true;
        mavlink_status = 'start streaming to ' + destination.host + ':' + destination.port;
        emit_status();

        console.log('starting mavlink to ' + destination.host + ':' + destination.port);


        const start_process = function(){
            console.log('starting mavlink process');

            mavlink_process = spawn("/home/pi/ground_radio/scripts/udp_mavlink_stream.sh", ['-h', destination.host, '-p', destination.port], {killSignal: 'SIGINT'});

            mavlink_process.stdout.on('data', (data) => {
                //console.log(`stdout: ${data}`);

                if( !process_confirmed ){
                    mavlink_start_in_progress = false;
                    mavlink_status = 'ACTIVE streaming to ' + destination.host + ':' + destination.port;
                    process_confirmed = true;
                    emit_status();
                    callback('OK');
                }

            });

            mavlink_process.stderr.on('data', (data) => {
                console.log(`stderr: ${data}`);

                if( !process_confirmed ){
                    mavlink_start_in_progress = false;
                    mavlink_status = 'error 0';
                    process_confirmed = true;
                    emit_status();
                    callback('error');
                }

            });

            mavlink_process.on('close', (code) => {
                console.log(`child process exited with code ${code}`);

                // code = 255 failed to start

                if( !process_confirmed ){
                    mavlink_start_in_progress = false;
                    process_confirmed = true;
                    callback('closed');
                }

                mavlink_status = (code > 1 ? 'error ' + code : 'stopped');
                emit_status();
            });

        };

        const restart_process = function(){
            console.log('stopping mavlink');

            mavlink_process.kill('SIGSTOP');
            kill(mavlink_process.pid, 'SIGKILL', function(){
                mavlink_process = null;
                setTimeout(start_process, 200);
            });
        };


        if( mavlink_process ){
            restart_process();
        }
        else {
            start_process();
        }
    }
    catch (e){
        mavlink_start_in_progress = false;
        mavlink_status = 'error 0';
        emit_status();
        callback('error');
    }



};


const stop_mavlink = function(callback){
    console.log('stopping mavlink');

    mavlink_status = 'stopped';

    if( mavlink_process ){
        mavlink_process.kill('SIGSTOP');
        kill(mavlink_process.pid, 'SIGKILL', function(){
            mavlink_process = null;
            emit_status();
            callback('OK');
        });
    }
    else {
        emit_status();
        callback('OK');
    }

};



const check_pivideo = function(){
    let pivideo = spawn("/usr/local/bin/pivideo", ['-q', 'all'], {killSignal: 'SIGINT'});

    pivideo.stdout.on('data', (data) => {
        console.log(`pivideo: ${data}`);
    });

};

check_pivideo();


//
// Старт этого скрипта

// Через секунду после запуска, автозапустить по сохраненным параметрам
setTimeout(function(){
    // Посмотреть куда транслировали телеметрию и видео в прошлый раз
    redisClient.get('streams_dst', function(err, dst){
        if( err ){
            console.log(err);
            return;
        }

        if( dst ){
            console.log('saved destinations');
            console.log(JSON.parse(dst));

            let streams_dst = JSON.parse(dst);

            if( streams_dst ){
                if( streams_dst.video_dst ) {
                    let video_host_port = streams_dst.video_dst.split(':');
                    if (video_host_port[0] && video_host_port[1]) {
                        start_video({host: video_host_port[0], port: video_host_port[1]});
                    }
                }

                if( streams_dst.mavlink_dst ) {
                    let mavlink_host_port = streams_dst.mavlink_dst.split(':');
                    if (mavlink_host_port[0] && mavlink_host_port[1]) {
                        start_mavlink({host: mavlink_host_port[0], port: mavlink_host_port[1]});
                    }
                }
            }
        }
    });
}, 1000);


// Проверить состояние WiFi
request.get('http://localhost:8080/status', function (err, res, body) {
    console.log('wifi check:');

    if( err ){
        console.log(err);
        return;
    }

    let data = JSON.parse(body);

    console.log(data.status);
    console.log(data.message);
    console.log(data);

});


setInterval(function(){
    request.get('http://localhost:8080/scan', function (err, res, body) {
        console.log('wifi check:');

        if( err ){
            console.log(err);
            return;
        }

        let data = JSON.parse(body);

        console.log(data);

    });
}, 5000);




//
//              WEB API TEST FIXME
//

// Старт скрипта
app.get('/api/test', function (req, res){

    // TODO
    // req.client_ip

    if( video_process ){
        console.log('script is already running');
        res.json({test: 'script is already running'});
    }
    else {
        console.log('executing script');
        video_process = spawn("/home/pi/ground_radio/scripts/udp_video.sh", ['-lh', '/usr'], {killSignal: 'SIGINT'});

        video_process.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });

        video_process.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
        });

        video_process.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });

        video_process.on('exit', (code, signal) => {
            console.log(`Exit code is: ${code}`);
            console.log(`Signal code is: ${signal}`);
        });

        res.json({test: 'script started OK'});
    }

});

// Остановка скрипта
app.get('/api/test2', function (req, res){

    if( video_process ){
        console.log('stopping script');

        //video_process.kill('SIGQUIT');
        //video_process.kill('SIGSTOP');
        //video_process.kill('SIGTERM');
        //video_process.kill('SIGKILL');
        //video_process.kill('SIGINT');
        //video_process.kill('SIGUSR1');
        //video_process.kill('SIGUSR2');
        //video_process.kill();

        //subprocess.kill();

        /*
        setTimeout(function(){
            if( !video_process || video_process.killed ){
                video_process=null;
                console.log('process killed');
            } else {
                console.log('process not killed');
            }

        }, 500);
        */


        video_process.kill('SIGSTOP');
        kill(video_process.pid, 'SIGKILL', function(){
            console.log('process killed 1');
            video_process = null;
        });

        res.json({test: 'stopping script'});
    }
    else {
        console.log('script not running');
        res.json({test: 'script not running'});
    }

});


// Включение реле (выключение питания)
app.get('/api/r1', function (req, res){

    //relay_video_tx.write(0, function(){});

    res.json({test: 'TX ON'});

});

// Выключение реле (включение питания)
app.get('/api/r0', function (req, res){

    //relay_video_tx.write(1, function(){});

    res.json({test: 'TX OFF'});

});





//
// Запуск сервера
server.listen(config.HTTP_SERVER_PORT, '0.0.0.0', () => {
    console.log('Listening on port ' + config.HTTP_SERVER_PORT);
});


//
// Web-приложение соединяется с сервером Socket.io
io_server.on('connection', function(io_client) {

    console.log('io client connected from ' + io_client.handshake.address);

    io_client.on('disconnect', function () {
        console.log('io client disconnected ' + io_client.handshake.address);
    });

    io_client.join('main_room');

    // Отправка состояния в веб-приложение
    io_client.emit('status', {
        video: video_status
        ,mavlink: mavlink_status
        ,client_ip: io_client.handshake.address
        ,video_rx: (video_rx_state === VIDEO_RX_STATE_ON ? 'On' : 'Off')
    });

    // Отправить адреса трансляций
    io_client.emit('streams_dst', {
        video_dst: io_client.handshake.address + ':5000'
        ,mavlink_dst: io_client.handshake.address + ':14550'
    });

    // Нажатие на кнопку Запуск трансляции
    io_client.on('btn_start', function(streams_dst, resp){
        console.log('streams:');
        console.log(streams_dst);

        // Сохранить параметры в Redis и стартануть скрипты
        redisClient.set('streams_dst', JSON.stringify(streams_dst), function(err){
            if( !err ) redisClient.bgsave();
        });

        // Ждем ответ от видео и от mavlink
        let v_st = null;
        let m_st = null;

        // Возвращаем ответ в браузер только, когда получены оба
        const response = function(){
            if( null !== v_st && null !== m_st ){
                resp({video: v_st, mavlink: m_st});
            }
        };

        // Если переданы параметры трансляции
        if( streams_dst ){

            // Видео
            if( streams_dst.video_dst ) {
                let video_host_port = streams_dst.video_dst.split(':');
                if (video_host_port[0] && video_host_port[1]) {
                    start_video({host: video_host_port[0], port: video_host_port[1]}, function (state) {
                        v_st = state;
                        response();
                    });
                }
            }
            else {
                v_st = 0;
            }

            // MAVLink
            if( streams_dst.mavlink_dst ) {
                let mavlink_host_port = streams_dst.mavlink_dst.split(':');
                if (mavlink_host_port[0] && mavlink_host_port[1]) {
                    start_mavlink({host: mavlink_host_port[0], port: mavlink_host_port[1]}, function (state) {
                        m_st = state;
                        response();
                    });
                }
            }
            else {
                m_st = 0;
            }
        }
        else {
            v_st = 0;
            m_st = 0;
        }

    });

    // нажатие на кнопку Stop
    io_client.on('btn_stop', function(streams_dst, resp){
        console.log('stop streams');

        // Ждем ответ от видео и от mavlink
        let v_st = null;
        let m_st = null;

        // Возвращаем ответ в браузер только, когда получены оба
        const response = function(){
            if( null !== v_st && null !== m_st ){
                resp({video: v_st, mavlink: m_st});
            }
        };

        stop_video(function(state){
            v_st = state;
            response();
        });

        stop_mavlink(function(state){
            m_st = state;
            response();
        });


    });




});



let led_blink = 0;

//
// Мигание светодиодом статуса
setInterval(function(){
    led_blink = (1 === led_blink ? 0 : 1);
    status_led.write(led_blink, function(){});
}, 500);


//
// остановка скрипта
process.on('SIGINT', function() {
    /*
        процедуры перед остановкой скрипта
     */

    let v_pr = 0;
    let m_pr = 0;

    const pr_exit = function(){
        if( v_pr === 1 && m_pr === 1 ){
            process.exit();
        }
    };

    redisClient.bgsave();

    if( video_process ){
        console.log('stopping video');

        video_process.kill('SIGSTOP');
        kill(video_process.pid, 'SIGKILL', function(){
            video_process = null;
            video_status = 'stopped';
            v_pr = 1;

            emit_status();

            pr_exit();
        });
    }
    else {
        v_pr = 1;
    }

    if( mavlink_process ){
        console.log('stopping mavlink');

        mavlink_process.kill('SIGSTOP');
        kill(mavlink_process.pid, 'SIGKILL', function(){
            mavlink_process = null;
            mavlink_status = 'stopped';
            m_pr = 1;

            emit_status();

            pr_exit();

        });
    }
    else {
        m_pr = 1;
    }

    pr_exit();



});





/*
function puts(error, stdout, stderr) {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
}
*/




