# Наземный радиомодуль

Инструкция по установке ПО на Raspberry Pi.
Установка с подключением кабелем к роутеру.



##  Установка

Найти Raspberry в локальной сети

    sudo nmap -sP 192.168.0.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'
    
или так

    arp -a | grep :    


Первоначальные настройки интерфейсов

    sudo raspi-config


Обновление дистрибутива

    sudo apt-get update && sudo apt-get upgrade -y
    
    
Установка вспомогательных пакетов    
    
    sudo apt-get install git wiringpi pigpio python-opencv python-wxgtk2.8 python-pip python-dev python-lxml python3-lxml python-smbus
    

Проверка модуля камеры

    vcgencmd get_camera
    
    
###   Задержка загрузки    
    
    sudo nano /boot/config.txt
    
Добавить в конец файла 

    # Boot delay for PiCapture to init
    boot_delay=5

    
    
    
    
###   Redis

    sudo apt-get install redis-server
    
После установки, Redis запускается сам. Проверка состояния:    
    
    sudo /etc/init.d/redis-server status

Файл конфигурации

    sudo nano /etc/redis/redis.conf

Перезапуск процесса

    sudo systemctl restart redis    
    
    

###   Gstreamer    
    
    sudo apt-get install gstreamer1.0-tools gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav
    

###   rpicamsrc

    sudo apt-get install autoconf automake libtool libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libraspberrypi-dev
    !
    git clone https://github.com/thaytan/gst-rpicamsrc.git rpicamsrc
    cd rpicamsrc
    chmod +x autogen.sh
    ./autogen.sh --prefix=/usr --libdir=/usr/lib/arm-linux-gnueabihf/
    make
    sudo make install

Проверка rpicamsrc:

    gst-inspect-1.0 rpicamsrc



###   PiCapture

    sudo apt-get install python-smbus
    sudo pip install pivideo

проверка:

    pivideo -q all -v



###   MAVProxy

    #sudo apt-get install python-opencv python-wxgtk2.8 python-pip python-dev python-lxml python3-lxml
    sudo pip install MAVProxy


###   Node JS

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs build-essential
    
    
###   Менеджер процессов PM2

    sudo npm install pm2 -g
    #pm2 install pm2-logrotate
    pm2 startup



###   Wifi клиент и точка доступа

Отсюда: https://imti.co/iot-wifi/

Отключить wpa_suppicant
    
    sudo systemctl mask wpa_supplicant.service
    sudo mv /sbin/wpa_supplicant /sbin/no_wpa_supplicant
    sudo pkill wpa_supplicant

Установка Docker

    curl -sSL https://get.docker.com | sh
    
Добавить пользователя pi в группу docker

    sudo usermod -aG docker pi

Перезагрузить

    sudo reboot

Подключиться вновь и проверить docker

    docker run --rm hello-world
    
Загрузить образ iotwifi

    docker pull cjimti/iotwifi
    
Файл конфигурации

    cd
    
    wget https://raw.githubusercontent.com/cjimti/iotwifi/master/cfg/wificfg.json        

Отредакировать файл конфигурации, написать ssid и пароль сети

    nano wificfg.json 


Проверка:

    docker run --rm --privileged --net host \
          -v $(pwd)/wificfg.json:/cfg/wificfg.json \
          cjimti/iotwifi


В другом окне терминала проверить интерфейсы

    ifconfig


Проверка через web-API

    curl http://localhost:8080/status


Запуск в фоновом режиме

    docker run -d --privileged --net host --restart=unless-stopped \
          -v $(pwd)/wificfg.json:/cfg/wificfg.json \
          cjimti/iotwifi


Соединение с WiFi сетью:

    curl -w "\n" -d '{"ssid":"Topolek43", "psk":"topolek7007"}' \
         -H "Content-Type: application/json" \
         -X POST localhost:8080/connect


###   drone_ground_radio

Скопировать код и запустить установку модулей

    npm install

Запустить скрипт

    node radio
    
Если все работает, включить автозапуск

    pm2 start radio.js
    pm2 save




Обычная передача напрямую клиенту

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.27.129 port=5000



Передача обратно в udp

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=127.0.0.1 port=5001


А потом клиенту

    gst-launch-1.0 udpsrc port=5001 ! udpsink host=192.168.27.129 port=5000





Чтение видеопотока из камеры и передача в UDP multicast

    (*) gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=239.255.255.250 port=5000 auto-multicast=true

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! udpsink host=239.255.255.250 port=5000 auto-multicast=true

    * gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 awb-mode=auto annotation-mode=time annotation-text-size=100 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=239.255.255.250 port=5000 auto-multicast=true

    gst-launch-1.0 rpicamsrc bitrate=1000000 preview=0 awb-mode=auto annotation-mode=time annotation-text-size=100 ! video/x-h264,width=1920,height=1080,framerate=30/1,profile=high ! videoconvert ! textoverlay text="Room A" valignment=top halignment=left font-desc="Sans, 72" ! videoconvert ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=239.255.255.250 port=5000 auto-multicast=true



Передача видеопотока из UDP multicast адреса локальному клиенту по UDP

    gst-launch-1.0 udpsrc multicast-group=239.255.255.250 auto-multicast=true port=5000 ! udpsink host=192.168.0.28 port=5000

    * gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! udpsink host=192.168.0.28 port=5000


Передача видеопотока из UDP multicast адреса в интернет на RTSP сервер

    * gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp ! rtph264depay ! flvmux ! rtmpsink location="rtmp://video.gcs.uno:1935/test11011/test1 live=1"



Попытка уменьшения размера трансляции в интернет

    gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp ! rtph264depay ! videoscale ! video/x-raw, width=960 height=540  ! flvmux ! rtmpsink location="rtmp://video.gcs.uno:1935/test11011/test1 live=1"

    gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp ! rtph264depay ! avdec_h264 ! decodebin caps=video/x-raw ! videoconvert ! video/x-raw,format=\(string\)RGB, width=960, height=540 ! videoscale ! videoconvert ! udpsink host=192.168.0.28 port=5005


Рабочий но глючный и через пару секунд отпадает

    gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp ! rtph264depay ! avdec_h264 ! x264enc tune=zerolatency ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5005 --verbose

Рабочий вариант с замедленным временем

    gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp, payload=96 ! rtpjitterbuffer ! rtph264depay ! avdec_h264 ! videoscale ! videoconvert ! video/x-raw,width=960,height=540,codec=h264,type=video,framerate=25/1 ! videoconvert ! x264enc tune=zerolatency speed-preset=ultrafast bitrate=100000 ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5005 --verbose

без этого параметра отваливается

    rtpjitterbuffer

    gst-launch-1.0 udpsrc uri=udp://239.255.255.250:5000 auto-multicast=true ! application/x-rtp, payload=96 ! rtph264depay ! avdec_h264 ! videoscale ! videoconvert ! video/x-raw,width=960,height=540,codec=h264,type=video ! x264enc tune=zerolatency speed-preset=ultrafast bitrate=100000 ! rtph264pay config-interval=1 pt=96 ! udpsink host=192.168.0.28 port=5005 --verbose








    ! 


    gst-inspect-1.0 avdec_h264



    gst-launch-1.0 -v udpsrc port=5000 ! gdpdepay ! rtph264depay ! decodebin ! videoconvert ! video/x-raw,format=\(string\)RGB ! videoconvert !




     video/x-raw, width=960, height=540,codec=h264, type=video  
    ! videoconvert ! videoscale ! video/x-raw,width=800,height=600 ! autovideosink


    multiudpsink clients=127.0.0.1:5000,127.0.0.1:5004,192.168.2.15:2000 
    sends it to three different destinations.


    gst-launch-1.0 rpicamsrc bitrate=1000000 ! video/x-h264,width=320,height=240,framerate=30/1 ! h264parse ! flvmux ! rtmpsink location="rtmp://video.gcs.uno:1935/test11011/test2 live=1"



https://imti.co/iot-wifi/

 curl -w "\n" -d '{"ssid":"183", "psk":"9652203000"}' \
     -H "Content-Type: application/json" \
     -X POST localhost:8080/connect








## Часы RTS DS3231 ##

В файл

    sudo nano /etc/modules

добавить

    snd-bcm2835
    i2c-bcm2835
    i2c-dev
    rtc-ds1307

подключить часы и проверить

    sudo i2cdetect -y 1

в файл

    sudo nano /bot/config.txt
    
добавить

    dtoverlay=i2c-rtc,ds3231    

Закомментировать в файле

    sudo nano /lib/udev/hwclock-set

эти строки

    #if [ -e /run/systemd/system ] ; then
    #    exit 0
    #fi

Перегрузить

    sudo reboot
    
Проверить

    sudo hwclock -r    


## Служебные команды Raspberry Pi


Температура процессора
    
    vcgencmd measure_temp
    

Проверка камеры

    vcgencmd get_camera
    
    






    