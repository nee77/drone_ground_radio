module.exports = {
    // GCS.uno MAVLink server
    //MAVLINK_SERVER: 'https://mavlink.gcs.uno:443'
    HTTP_SERVER_PORT: 8091

    // Local UDP host and port to which MAVProxy streams messages from Ardupilot
    //,MAVLINK_LOCAL_UDP_HOST: '127.0.0.1'
    //,MAVLINK_LOCAL_UDP_PORT: 15001



    //,API_SERVER_PORT: 8095

    ,REDIS_HOST: 'localhost'
    ,REDIS_PORT: 6379

    ,IO_HOST: 'http://localhost:8091'  // сервер управления

};